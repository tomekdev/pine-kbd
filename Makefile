OBJS=pine-kbd.o \
     backlight.o

CPPFLAGS=
LDFLAGS=

all: $(OBJS)
	g++ -o pine-kbd $(OBJS) $(CPPFLAGS) $(LDFLAGS)

clean:
	rm -rf $(OBJS) pine-kbd
