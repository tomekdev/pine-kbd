#include <string>
#include <fstream>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <cstdlib>
#include <linux/input.h>
#include "backlight.h"
#include "types.h"

/* Pinebook Pro keyboard input node */
#define PINEBOOOK_KBD_INPUT_PATH	"/dev/input/by-id/usb-Pine64_Pinebook_Pro_ANSI_0.1-event-kbd"

/* Keycodes */
#define BRIGHTNESSUP_KEY_CODE	60
#define BRIGHTNESSDOWN_KEY_CODE	59
#define PINE_KEY_CODE		125

using namespace std;

bool die = false;

/* Our "function" key state */
bool pine_button_pressed = false;

void interpret_event(struct input_event event)
{
	/* Using Pine button + F2/F3 because Fn + F2/F3 give the same keycode */
	if(event.code == PINE_KEY_CODE && event.type == EV_KEY && event.value == 1)
		pine_button_pressed = true;

	if(event.code == PINE_KEY_CODE && event.type == EV_KEY && event.value == 0)
		pine_button_pressed = false;

	if(event.code == BRIGHTNESSUP_KEY_CODE && event.type == EV_KEY && event.value == 1 && pine_button_pressed)
	{
		struct backlight_properties prop;
		if(get_backlight_properties(prop) != 0) return;

		brightnessup(prop, 10);
	}

	if(event.code == BRIGHTNESSDOWN_KEY_CODE && event.type == EV_KEY && event.value == 1 && pine_button_pressed)
	{
		struct backlight_properties prop;
		if(get_backlight_properties(prop) != 0) return;

		brightnessdown(prop, 10);
	}
}

int main()
{
	FILE *kbfd = fopen(PINEBOOOK_KBD_INPUT_PATH, "rb");
	if(kbfd)
	{
		struct input_event buf;
		while(!die)
		{
			int bytes_read = fread(&buf, 1, sizeof(struct input_event), kbfd);

			if(bytes_read == 0)
			{
				fclose(kbfd);
				kbfd = fopen(PINEBOOOK_KBD_INPUT_PATH, "rb");
			}
			else
				interpret_event(buf);
		}
		fclose(kbfd);
	}
	else
	{
		cout << "Failed to open " << PINEBOOOK_KBD_INPUT_PATH << " for reading: " << strerror(errno) << endl;
	}
	return 0;
}
